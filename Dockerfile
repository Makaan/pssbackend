FROM node:12.18.1

ENV NODE_ENV=production
ENV DB_USER=admin
ENV DB_PASS=mongoVPS
ENV DB_PATH=mongodb://93.188.165.12/pssDb?authSource=admin

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --production

COPY . .

CMD [ "node", "src/server.js" ]
