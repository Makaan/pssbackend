#!/bin/bash

echo "Frenando container..."
docker stop pss
echo "Borrando container..."
docker rm pss
echo "Reconstruyendo container..."
docker build -t pssbackend:0.1 .
echo "Ejecutando nuevo container..."
docker run -dp 8081:8081 --name pss pssbackend:0.1
echo "Terminado."
