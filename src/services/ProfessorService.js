const Container = require('typedi').Container

module.exports = class {
  constructor () {
    this.UserModel = Container.get('UserModel')
    this.SubjectModel = Container.get('SubjectModel')
  }

  async addGradeToStudent (subjectId, studentId, grade) {
    try {
      let resu = null
      if (grade < 4) {
        resu = await this.UserModel.update({
          _id: studentId,
          'approvedSubjects.materia': { $ne: subjectId }
        },
        {
          $push: { approvedSubjects: { materia: subjectId, nota: grade } }
        })
      } else {
        resu = await Promise.all([
          this.UserModel.update({
            _id: studentId,
            'approvedSubjects.materia': { $ne: subjectId }
          },
          {
            $push: { approvedSubjects: { materia: subjectId, nota: grade } },
            $pull: { currentSubjects: subjectId }
          }),
          this.SubjectModel.findOneAndUpdate(
            { _id: subjectId },
            {
              $pull: { students: studentId }
            }
          )
        ])
      }
      console.log('resu pre update', resu)
      if (resu.nModified === 0 || (resu.hasOwnProperty('length') && resu[0].nModified === 0)) {
        console.log('update')
        const promiseArr = [
          this.updateGradeOfStudent(subjectId, studentId, grade)
        ]
        if (grade >= 4) {
          promiseArr.push(
            this.UserModel.update({
              _id: studentId
            },
            {
              $pull: { currentSubjects: subjectId }
            })
          )
        }
        return await Promise.all(promiseArr)
      }

      return resu
    } catch (err) {
      console.log(err)
    }
  }

  async updateGradeOfStudent (subjectId, studentId, grade) {
    try {
      return this.UserModel.update({
        _id: studentId,
        approvedSubjects: { $elemMatch: { materia: subjectId } }
      },
      {
        $set: { 'approvedSubjects.$.nota': grade }
      })
    } catch (err) {
      console.log(err)
    }
  }

  async removeGradeToStudent (subjectId, studentId) {
    try {
      return this.UserModel.update({
        _id: studentId,
        approvedSubjects: { $elemMatch: { materia: subjectId } }
      },
      {
        $pull: { approvedSubjects: { materia: subjectId } }
      })
    } catch (err) {
      console.log(err)
    }
  }

  async addProfessorToSubject (subjectId, professorId) {
    try {
      await Promise.all([
        this.UserModel.findOneAndUpdate({ _id: professorId }, {
          $addToSet: { assignedSubjectsAsProfessor: subjectId }
        }),
        this.SubjectModel.findOneAndUpdate({ _id: subjectId }, { professor: professorId })
      ])
      return 'Agregada materia a estudiante.'
    } catch (err) {
      throw err || new Error('Error al agregar la materia al estudiante.')
    }
  }

  async addAssistantToSubject (subjectId, professorId) {
    try {
      const resu = await Promise.all([
        this.UserModel.findOneAndUpdate({ _id: professorId }, {
          $addToSet: { assignedSubjectsAsAssistant: subjectId }
        }),
        this.SubjectModel.findOneAndUpdate({ _id: subjectId }, { assistant: professorId })
      ])
      console.log(resu)
      return 'Agregada materia a estudiante.'
    } catch (err) {
      throw err || new Error('Error al agregar la materia al estudiante.')
    }
  }

  async removeProfessorFromSubject (subjectId, professorId) {
    try {
      await Promise.all([
        this.UserModel.findOneAndUpdate({ _id: professorId }, {
          $pull: { assignedSubjectsAsProfessor: subjectId }
        }),
        this.SubjectModel.findOneAndUpdate({ _id: subjectId }, { professor: undefined })
      ])
      return 'Agregada materia a estudiante.'
    } catch (err) {
      throw err || new Error('Error al agregar la materia al estudiante.')
    }
  }

  async removeAssistantFromSubject (subjectId, professorId) {
    try {
      await Promise.all([
        this.UserModel.findOneAndUpdate({ _id: professorId }, {
          $pull: { assignedSubjectsAsAssistant: subjectId }
        }),
        this.SubjectModel.findOneAndUpdate({ _id: subjectId }, { assistant: undefined })
      ])
      return 'Agregada materia a estudiante.'
    } catch (err) {
      throw err || new Error('Error al agregar la materia al estudiante.')
    }
  }
}
