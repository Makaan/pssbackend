const Container = require('typedi').Container

module.exports = class CareerService {
  constructor () {
    this.CareerModel = Container.get('CareerModel')
  }

  async createCareer (careerData) {
    try {
      const err = this.careerValidation(careerData)
      if (err) {
        throw err
      }
      const doc = await this.CareerModel.findOne({
        name: careerData.name
      })
      if (doc) {
        throw new Error('La Carrera ya existe')
      }
      await this.CareerModel.create(careerData)
      return 'Carrera creada exitosamente'
    } catch (err) {
      console.log('createSubject', err)
      throw err || new Error('Ocurrio un error al crear el usuario')
    }
  }

  async findCareer (name) {
    try {
      return this.CareerModel.findOne({ name: name })
        .populate('academicUnit')
        .populate({
          path: 'subjects',
          populate: {
            path: 'professor',
            model: 'User'
          }
        })
    } catch (err) {
      console.log('findCareer', err)
      throw new Error('Ocurrio un error al buscar la unidad academica')
    }
  }

  async findCareerById (id) {
    try {
      return this.CareerModel.findOne({ _id: id })
        .populate('academicUnit')
        .populate({
          path: 'subjects',
          populate: {
            path: 'professor',
            model: 'User'
          }
        })
    } catch (err) {
      console.log('findCareer', err)
      throw new Error('Ocurrio un error al buscar la unidad academica')
    }
  }

  async findAllCareers () {
    try {
      return this.CareerModel.find()
        .populate('academicUnit')
        .populate({
          path: 'subjects',
          populate: [{
            path: 'professor',
            model: 'User'
          },
          {
            path: 'assistant',
            model: 'User'
          }]

        })
    } catch (err) {
      console.log('findCareer', err)
      throw new Error('Ocurrio un error al buscar las unidades academicas.')
    }
  }

  async updateCareer (careerData) {
    try {
      await this.CareerModel.findOneAndUpdate({ _id: careerData._id }, careerData)
      return 'Carrera actualizada con exito'
    } catch (err) {
      console.log(err)
      throw new Error('Ocurrio un error al actualizar la carrera')
    }
  }

  async removeCareer (name) {
    try {
      await this.CareerModel.remove({ name: name })
      return 'Carrera borrada con exito.'
    } catch (err) {
      console.log('removeUser', err)
      throw new Error('Ocurrio un error al borrar la Carrera.')
    }
  }

  careerValidation (careerData) {
    if (careerData.name === undefined || careerData.name === '') {
      return new Error('No se suministró un nombre para la carrera.')
    }
    if (careerData.academicUnit.length === 0) {
      return new Error('Se necesita tener asociada al menos una unidad academica')
    }
  }

  async addSubjectToCareers (careersIds, subjectId) {
    if (!careersIds || careersIds.length === 0) { return }

    const promiseArr = []
    for (let i = 0; i < careersIds.length; i++) {
      promiseArr.push(
        this.CareerModel.update({ _id: careersIds[i] }, {
          $addToSet: { subjects: subjectId }
        })
      )
    }
    return Promise.all(promiseArr)
  }

  async removeSubjectFromCareers (careersIds, subjectId) {
    const promiseArr = []
    for (let i = 0; i < careersIds.length; i++) {
      promiseArr.push(
        this.CareerModel.update({ _id: careersIds[i] }, {
          $pull: { subjects: subjectId }
        })
      )
    }
    return Promise.all(promiseArr)
  }
}
