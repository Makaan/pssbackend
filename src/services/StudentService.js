const Container = require('typedi').Container

module.exports = class StudentService {
  constructor () {
    this.CareerService = Container.get('CareerService')
    this.SubjectService = Container.get('SubjectService')
    this.UserService = Container.get('UserService')
  }

  async enrollInSubject (studentId, subjectId) {
    try {
      const promiseArr = [
        this.UserService.addSubjectToStudent(studentId, subjectId),
        this.SubjectService.addStudentToSubject(studentId, subjectId)
      ]
      await Promise.all(promiseArr)
      return 'ok'
    } catch (err) {
      throw err || new Error('No se pudo inscribir al estudiante')
    }
  }

  async dissenrollFromSubject (studentId, subjectId) {
    try {
      console.log('dfs', studentId, subjectId)
      const resp = await Promise.all([
        this.UserService.removeSubjectFromStudent(studentId, subjectId),
        this.SubjectService.removeStudentFromSubject(studentId, subjectId)
      ])
      console.log(resp)
      return 'ok'
    } catch (err) {
      console.log('dfs', err)
      throw err || new Error('No se pudo inscribir al estudiante')
    }
  }

  async getAllPossibleSubjects (student) {
    try {
      const career = await this.CareerService.findCareer(student.career.name)
      const availableSubjects = []
      for (const subject of career.subjects) {
        // Veo si ya está anotado
        let enrolled = false
        for (const currentSubject of student.currentSubjects) {
          if (subject._id.equals(currentSubject._id)) {
            enrolled = true
            break
          }
        }
        // Veo si ya está aprobada
        let approved = false
        for (const approvedSubject of student.approvedSubjects) {
          if (subject._id.equals(approvedSubject.materia._id)) {
            approved = true
            break
          }
        }
        if (!enrolled && !approved) {
          if (!subject.strongCorrelatives || subject.strongCorrelatives.length === 0) {
            availableSubjects.push(subject)
          } else {
            // Si no esta anotado tengo que ver que cumpla las correlativas
            let tieneCorrelativas = true
            for (const correlativa of subject.strongCorrelatives) {
              // console.log('correlativa', correlativa)
              let esta = false
              for (const aprobada of student.approvedSubjects) {
                // console.log('aprobada', aprobada.materia._id)
                // console.log('check', correlativa.equals(aprobada.materia._id))
                if (correlativa.equals(aprobada.materia._id)) {
                  esta = true
                }
              }
              if (!esta) {
                tieneCorrelativas = false
                break
              }
            }
            if (tieneCorrelativas) {
              availableSubjects.push(subject)
            }
          }

          // availableSubjects.push(subject)
        }
      }
      const promiseArr = []
      for (const subject of availableSubjects) {
        promiseArr.push(
          this.SubjectService.findSubjectById(subject)
        )
      }
      return await Promise.all(promiseArr)
    } catch (err) {
      console.log('getAllPossibleSubjects', err)
      throw new Error('Ocurrieron errores al buscar las materias.')
    }
  }
}
