const bcrypt = require('bcrypt')
const saltRounds = 10

module.exports = class {
  async generateHash (password) {
    return bcrypt.hash(password, saltRounds)
  }

  generateHashSync (password) {
    return bcrypt.hashSync(password, saltRounds)
  }

  async checkPassword (password, hash) {
    return bcrypt.compare(password, hash)
  }
}
