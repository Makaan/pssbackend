const Container = require('typedi').Container

module.exports = class UserService {
  constructor () {
    try {
      this.UserModel = Container.get('UserModel')
      this.PasswordEncyptionService = Container.get('PasswordEncryptionService')
    } catch (err) {
      console.log(err)
    }
  }

  async createUser (userData) {
    console.log('createUser', userData)
    return new Promise((resolve, reject) => {
      try {
        const err = this.userValidation(userData)
        console.log('errCreateUser', err)
        if (err) {
          reject(err)
          return
        }
        if (userData.password === undefined || userData.password === '') {
          return new Error('No se suministró una contraseña para el usuario')
        }

        const promiseArr = [
          this.UserModel.findOne({ username: userData.username }),
          this.UserModel.findOne({ dni: userData.dni })
        ]

        if (userData.lu) {
          delete userData.lu
        }

        Promise.all(promiseArr).then(docs => {
          console.log(docs)
          if (docs[0]) {
            throw new Error('El usuario ya existe.')
          }
          if (docs[1]) {
            throw new Error('Ya existe un usuario con ese DNI.')
          }

          return this.PasswordEncyptionService.generateHash(userData.password)
        })
          .then(pass => {
            userData.password = pass
            console.log(userData)
            return this.UserModel.create(userData)
          })
          .then(() => resolve('Usuario creado exitosamente'))
          .catch(err => {
            console.log('createUser', err)
            reject(err || new Error('Ocurrio un error al crear el usuario'))
          })
      } catch (err) {
        reject(err)
      }
    })
  }

  async findUser (username) {
    try {
      return await this.UserModel.findOne({ username: username })
        .populate({
          path: 'currentSubjects',
          populate: [{
            path: 'professor',
            model: 'User'
          },
          {
            path: 'assistant',
            model: 'User'
          }]
        })
        .populate({
          path: 'approvedSubjects',
          populate: {
            path: 'materia',
            model: 'Subject'
          }
        })
        .populate('career')
        .populate('nonApprovedSubjects')
        .populate({
          path: 'assignedSubjectsAsProfessor',
          populate: {
            path: 'students',
            model: 'User'
          }
        })
        .populate({
          path: 'assignedSubjectsAsAssistant',
          populate: {
            path: 'students',
            model: 'User'
          }
        })
    } catch (err) {
      console.log(err)
      throw new Error('No se pudo buscar al usuario.')
    }
  }

  async findUserById (id) {
    try {
      return await this.UserModel.findOne({ _id: id })
        .populate({
          path: 'currentSubjects',
          populate: [{
            path: 'professor',
            model: 'User'
          },
          {
            path: 'assistant',
            model: 'User'
          }]
        })
        .populate({
          path: 'approvedSubjects',
          populate: {
            path: 'materia',
            model: 'Subject'
          }
        })
        .populate('career')
        .populate('approvedSubjects')
        .populate('nonApprovedSubjects')
        .populate({
          path: 'assignedSubjectsAsProfessor',
          populate: {
            path: 'students',
            model: 'User'
          }
        })
        .populate({
          path: 'assignedSubjectsAsAssistant',
          populate: {
            path: 'students',
            model: 'User'
          }
        })
    } catch (err) {
      console.log(err)
      throw new Error('No se pudo buscar al usuario.')
    }
  }

  async findAllUsers () {
    return new Promise((resolve, reject) => {
      this.UserModel.find()
        .populate({
          path: 'currentSubjects',
          populate: [{
            path: 'professor',
            model: 'User'
          },
          {
            path: 'assistant',
            model: 'User'
          }]
        })
        .populate('career')
        .populate('approvedSubjects')
        .populate('nonApprovedSubjects')
        .populate({
          path: 'assignedSubjectsAsProfessor',
          populate: {
            path: 'students',
            model: 'User'
          }
        })
        .populate({
          path: 'assignedSubjectsAsAssistant',
          populate: {
            path: 'students',
            model: 'User'
          }
        })
        .then(docs => {
          resolve(docs)
        })
        .catch(err => {
          console.log(err)
          reject(new Error('Ocurrio un error al buscar los usuarios'))
        })
    })
  }

  async findAllStudents () {
    return new Promise((resolve, reject) => {
      this.UserModel.find({ role: 'student' })
        .populate({
          path: 'currentSubjects',
          populate: [{
            path: 'professor',
            model: 'User'
          },
          {
            path: 'assistant',
            model: 'User'
          }]
        })
        .populate({
          path: 'approvedSubjects',
          populate: {
            path: 'materia',
            model: 'Subject'
          }
        })
        .populate('career')
        .populate('nonApprovedSubjects')
        .populate('assignedSubjects')
        .then(docs => {
          resolve(docs)
        })
        .catch(err => {
          console.log(err)
          reject(new Error('Ocurrio un error al buscar los usuarios'))
        })
    })
  }

  async findAllProfessors () {
    return new Promise((resolve, reject) => {
      this.UserModel.find({ role: 'professor' })
        .populate({
          path: 'assignedSubjectsAsProfessor',
          populate: {
            path: 'students',
            model: 'User'
          }
        })
        .populate({
          path: 'assignedSubjectsAsAssistant',
          populate: {
            path: 'students',
            model: 'User'
          }
        })
        .then(docs => {
          resolve(docs)
        })
        .catch(err => {
          console.log(err)
          reject(new Error('Ocurrio un error al buscar los usuarios'))
        })
    })
  }

  async updateUser (userData) {
    return new Promise((resolve, reject) => {
      console.log('updateUser data', userData)
      const err = this.updateUserValidation(userData)
      console.log('updateUser', err)
      if (err) {
        reject(err)
        return
      }
      if (userData.password) {
        userData.password = this.PasswordEncyptionService.generateHashSync(userData.password)
      }
      this.UserModel.findOneAndUpdate({ _id: userData._id }, userData)
        .then(
          resolve('Usuario actualizado con exito')
        )
        .catch(err => {
          console.log(err)
          reject(new Error('Ocurrio un error al actualizar el usuario'))
        })
    })
  }

  async removeUser (username) {
    return new Promise((resolve, reject) => {
      this.UserModel.remove({ username: username })
        .then(
          resolve('Usuario borrado con exito.')
        )
        .catch(err => {
          console.log('removeUser', err)
          reject(new Error('Ocurrio un error al borrar el usuario.'))
        })
    })
  }

  userValidation (userData) {
    if (userData.username === undefined || userData.username === '') {
      return new Error('No se suministró un nombre de usuario')
    }
    if (userData.dni < 0) {
      return new Error('El numero de documento no puede ser menor a cero.')
    }
    if (userData.lu < 0) {
      return new Error('El LU no puede ser menor a cero.')
    }
    return null
  }

  updateUserValidation (userData) {
    if (userData.dni < 0) {
      return new Error('El numero de documento no puede ser menor a cero.')
    }
    if (userData.lu) {
      return new Error('El LU no se puede modificar')
    }
  }

  async addSubjectToStudent (studentId, subjectId) {
    try {
      await this.UserModel.findOneAndUpdate({ _id: studentId }, {
        $addToSet: { currentSubjects: subjectId }
      })
      return 'Agregada materia a estudiante.'
    } catch (err) {
      throw err || new Error('Error al agregar la materia al estudiante.')
    }
  }

  async removeSubjectFromStudent (studentId, subjectId) {
    try {
      await this.UserModel.findOneAndUpdate({ _id: studentId }, {
        $pull: { currentSubjects: subjectId }
      })
      return 'Eliminado materia del estudiante.'
    } catch (err) {
      throw err || new Error('Error al eliminar la materia del estudiante.')
    }
  }
}
