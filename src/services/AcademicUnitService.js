const Container = require('typedi').Container

module.exports = class AcademicUnitService {
  constructor () {
    this.AcademicUnitModel = Container.get('AcademicUnitModel')
  }

  async createAcademicUnit (academicUnitData) {
    try {
      if (academicUnitData.name === undefined || academicUnitData.name === '') {
        throw new Error('No se suministró un nombre para la unidad academica.')
      }

      const doc = await this.AcademicUnitModel.findOne({
        name: academicUnitData.name
      })
      console.log('createAcademicUnit', doc)
      if (doc) {
        throw new Error('La Unidad Academica ya existe')
      }

      await this.AcademicUnitModel.create(academicUnitData)

      return 'Unidad Academica creada exitosamente'
    } catch (err) {
      console.log('createAcademicUnit', err)
      throw err
    }
  }

  async findAcademicUnit (name) {
    try {
      return await this.AcademicUnitModel.findOne({ name: name })
    } catch (err) {
      console.log('findAcademicUnit', err)
      throw new Error(err || 'Ocurrio un error al buscar la unidad academica')
    }
  }

  async findAcademicUnitById (id) {
    try {
      return await this.AcademicUnitModel.findOne({ _id: id })
    } catch (err) {
      console.log('findAcademicUnit', err)
      throw new Error(err || 'Ocurrio un error al buscar la unidad academica')
    }
  }

  async findAllAcademicUnits () {
    try {
      return await this.AcademicUnitModel.find()
        .populate('courses')
    } catch (err) {
      console.log('findAllAcademicUnits', err)
      throw err || new Error('Ocurrio un error al buscar las unidades academicas.')
    }
  }

  async updateAcademicUnit (academicUnitData) {
    const doc = await this.AcademicUnitModel.findOne({
      name: academicUnitData.name
    })
    if (doc) {
      throw new Error('No se puede actualizar una unidad academica con un nombre de otra que ya existe')
    }

    return await this.AcademicUnitModel.findOneAndUpdate({ _id: academicUnitData._id }, academicUnitData)
  }

  async removeAcademicUnit (name) {
    try {
      const resu = await this.AcademicUnitModel.deleteOne({ name: name })
      if (resu.deletedCount === 0) {
        throw new Error('No se pudo borrar ninguna unidad academica con ese nombre')
      }
      return 'Unidad Academica borrada con exito.'
    } catch (err) {
      console.log('removeUser', err)
      throw err || new Error('Ocurrio un error al borrar la Unidad Academica.')
    }
  }
}
