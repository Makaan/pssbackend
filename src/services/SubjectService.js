const Container = require('typedi').Container

module.exports = class SubjectService {
  constructor () {
    this.SubjectModel = Container.get('SubjectModel')
    this.CareerService = Container.get('CareerService')
  }

  async createSubject (subjectData) {
    try {
      const err = this.subjectValidation(subjectData)
      if (err) {
        throw err
      }

      let doc = await this.SubjectModel.findOne({
        name: subjectData.name
      })
      console.log('createSubject')
      if (doc) {
        throw new Error('La materia ya existe')
      }

      doc = await this.SubjectModel.create(subjectData)
      console.log(doc)
      await this.CareerService.addSubjectToCareers(subjectData.careers, doc._id)

      return 'Materia creada exitosamente'
    } catch (err) {
      console.log('createSubject', err)
      throw err || new Error('Ocurrio un error al crear la materia')
    }
  }

  async findSubject (name) {
    try {
      return await this.SubjectModel.findOne({ name: name })
        .populate('academicUnit')
        .populate('professor')
        .populate('assistant')
        .populate('careers')
        .populate('weakCorrelatives')
        .populate('strongCorrelatives')
        .populate('students')
    } catch (err) {
      console.log('findSubject', err)
      throw new Error('Ocurrieron errores al buscar la materia.')
    }
  }

  async findSubjectById (id) {
    try {
      return this.SubjectModel.findOne({ _id: id })
        .populate('academicUnit')
        .populate('professor')
        .populate('assistant')
        .populate('careers')
        .populate('weakCorrelatives')
        .populate('strongCorrelatives')
        .populate('students')
    } catch (err) {
      console.log('findSubject', err)
      return new Error('Ocurrieron errores al buscar la materia.')
    }
  }

  async findAllSubjects () {
    try {
      return this.SubjectModel.find()
        .populate('academicUnit')
        .populate('professor')
        .populate('assistant')
        .populate('careers')
        .populate('weakCorrelatives')
        .populate('strongCorrelatives')
        .populate('students')
    } catch (err) {
      console.log('findSubject', err)
      throw new Error('Ocurrieron errores al buscar las materias.')
    }
  }

  async updateSubject (subjectData) {
    try {
      await this.SubjectModel.findOneAndUpdate({ _id: subjectData._id }, subjectData)
      return 'Materia actualizada con exito'
    } catch (err) {
      console.log(err)
      throw new Error('Ocurrio un error al actualizar la materia')
    }
  }

  async removeSubject (name) {
    return new Promise((resolve, reject) => {
      this.SubjectModel.findOne({ name: name })
        .then(doc => {
          return Promise.all([
            this.CareerService.removeSubjectFromCareers(doc.careers, doc._id),
            this.SubjectModel.remove({ name: name })
          ])
        })
        .then(
          resolve('Materia borrada con exito.')
        )
        .catch(err => {
          console.log('removeSubject', err)
          reject(new Error('Ocurrio un error al borrar la Materia.'))
        })
    })
  }

  subjectValidation (subjectData) {
    if (subjectData.name === undefined || subjectData.name === '') {
      return new Error('No se suministró un nombre para la materia.')
    }
    if (subjectData.length === 0) {
      return new Error('Las materias necesitan una o mas unidades academicas asociadas.')
    }
    return null
  }

  async getAllSubjectsInCareer (career) {
    try {
      return await this.SubjectModel.find({ career: career })
        .populate('academicUnit')
        .populate('professor')
        .populate('assistant')
        .populate('career')
        .populate('weakCorrelatives')
        .populate('strongCorrelatives')
    } catch (err) {
      console.log('gettAllSubjectsInCareer')
      throw new Error(err || 'Ocurrieron errores al buscar las materias')
    }
  }

  async addStudentToSubject (studentId, subjectId) {
    try {
      const resps = await this.SubjectModel.findOneAndUpdate({ _id: subjectId }, {
        $addToSet: { students: studentId }
      })
      console.log(resps)
      return 'Agregada estudiante a materia.'
    } catch (err) {
      throw err || new Error('Error al agregar el estudiante a la materia.')
    }
  }

  async removeStudentFromSubject (studentId, subjectId) {
    try {
      await this.SubjectModel.findOneAndUpdate({ _id: subjectId }, {
        $pull: { students: studentId }
      })
      return 'Eliminado estudiante de materia.'
    } catch (err) {
      throw err || new Error('Error al eliminar el estudiante de la materia.')
    }
  }
}
