const Container = require('typedi').Container
const passport = require('passport')
const path = require('path')
const apiRoot = '/api/v1'

module.exports = (app) => {
  const UserService = Container.get('UserService')
  const CareerService = Container.get('CareerService')
  const SubjectService = Container.get('SubjectService')
  const AcademicUnitService = Container.get('AcademicUnitService')
  const StudentService = Container.get('StudentService')
  const ProfessorService = Container.get('ProfessorService')

  const loggedInOnly = (req, res, next) => {
    if (req.isAuthenticated()) {
      next()
    } else {
      res.status(401).send('Acceso no autorizado')
    }
  }

  const adminOnly = (req, res, next) => {
    if (req.user.role === 'admin') {
      next()
    } else {
      res.status(401).send('Acceso no autorizado')
    }
  }

  const studentOnly = (req, res, next) => {
    if (req.user.role === 'student') {
      next()
    } else {
      res.status(401).send('Acceso no autorizado')
    }
  }

  const professorOnly = (req, res, next) => {
    if (req.user.role === 'professor') {
      next()
    } else {
      res.status(401).send('Acceso no autorizado')
    }
  }

  app.get('/', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../build/index.html'))
  })

  //* Llamadas a la API de datos

  app.get(`${apiRoot}/getLoggedUser`, async (req, res) => {
    try {
      const user = await UserService.findUser(req.session.passport.user)
      res.send(user)
    } catch (err) {
      res.status(500).send(err)
    }
  })

  //* ------------------------------------------------------------------------------------------------------------------------
  //* Llamadas CRUD de usuario
  //* ------------------------------------------------------------------------------------------------------------------------

  app.post(`${apiRoot}/createUser`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      console.log('/createUser')
      const resp = await UserService.createUser(req.body)
      res.send(resp)
    } catch (err) {
      console.log('ERR API', err)
      res.status(500).send(err)
    }
  })

  app.get(`${apiRoot}/findUser`, loggedInOnly, async (req, res) => { // nestorfigliuolo.com.ar/pss/api/v1/findUser?username=pepito
    try {
      res.send(await UserService.findUser(req.query.username))
    } catch (err) {
      console.log(err)
      res.send(err)
    }
  })

  app.get(`${apiRoot}/findUserById`, loggedInOnly, async (req, res) => { // nestorfigliuolo.com.ar/pss/api/v1/findUser?username=pepito
    try {
      res.send(await UserService.findUserById(req.query.id))
    } catch (err) {
      console.log(err)
      res.send(err)
    }
  })

  app.get(`${apiRoot}/getAllUsers`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await UserService.findAllUsers())
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  })

  app.get(`${apiRoot}/getAllStudents`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await UserService.findAllStudents())
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  })

  app.get(`${apiRoot}/getAllProfessors`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await UserService.findAllProfessors())
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/updateUser`, loggedInOnly, async (req, res) => {
    try {
      res.send(await UserService.updateUser(req.body))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/removeUser`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await UserService.removeUser(req.body.username))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  //* ------------------------------------------------------------------------------------------------------------------------
  //* Llamadas de estudiantes
  //* ------------------------------------------------------------------------------------------------------------------------

  app.post(`${apiRoot}/enrollInSubject`, loggedInOnly, async (req, res) => {
    try {
      console.log('api enrollinsubject')
      res.send(await StudentService.enrollInSubject(req.body.studentId, req.body.subjectId))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/getAllPossibleSubjects`, loggedInOnly, async (req, res) => {
    try {
      res.send(await StudentService.getAllPossibleSubjects(req.body))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/dissenrollFromSubject`, loggedInOnly, async (req, res) => {
    try {
      console.log('api dissenrollFromSubject', req.body)
      res.send(await StudentService.dissenrollFromSubject(req.body.studentId, req.body.subjectId))
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  })

  //* ------------------------------------------------------------------------------------------------------------------------
  //* Llamadas de profesores
  //* ------------------------------------------------------------------------------------------------------------------------
  app.post(`${apiRoot}/addGradeToStudent`, loggedInOnly, async (req, res) => {
    try {
      res.send(await ProfessorService.addGradeToStudent(req.body.subjectId, req.body.studentId, req.body.grade))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/removeGradeToStudent`, loggedInOnly, async (req, res) => {
    try {
      res.send(await ProfessorService.removeGradeToStudent(req.body.subjectId, req.body.studentId))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/addProfessorToSubject`, loggedInOnly, async (req, res) => {
    try {
      res.send(await ProfessorService.addProfessorToSubject(req.body.subjectId, req.body.professorId))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/addAssistantToSubject`, loggedInOnly, async (req, res) => {
    try {
      res.send(await ProfessorService.addAssistantToSubject(req.body.subjectId, req.body.assistantId))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/removeProfessorFromSubject`, loggedInOnly, async (req, res) => {
    try {
      res.send(await ProfessorService.removeProfessorFromSubject(req.body.subjectId, req.body.professorId))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/removeAssistantFromSubject`, loggedInOnly, async (req, res) => {
    try {
      res.send(await ProfessorService.removeAssistantFromSubject(req.body.subjectId, req.body.assistantId))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  //* ------------------------------------------------------------------------------------------------------------------------
  //* Llamadas CRUD de materias
  //* ------------------------------------------------------------------------------------------------------------------------

  app.post(`${apiRoot}/createSubject`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await SubjectService.createSubject(req.body))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.get(`${apiRoot}/findSubject`, loggedInOnly, async (req, res) => { // nestorfigliuolo.com.ar/pss/api/v1/findSubject?Subjectname=pepito
    try {
      res.send(await SubjectService.findSubject(req.query.name))
    } catch (err) {
      console.log(err)
      res.send(err)
    }
  })

  app.get(`${apiRoot}/findSubjectById`, loggedInOnly, async (req, res) => { // nestorfigliuolo.com.ar/pss/api/v1/findSubject?Subjectname=pepito
    try {
      res.send(await SubjectService.findSubjectById(req.query.id))
    } catch (err) {
      console.log(err)
      res.send(err)
    }
  })

  app.get(`${apiRoot}/getAllSubjects`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await SubjectService.findAllSubjects())
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/updateSubject`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await SubjectService.updateSubject(req.body))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/removeSubject`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await SubjectService.removeSubject(req.body.name))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  //* ------------------------------------------------------------------------------------------------------------------------
  //* Llamadas CRUD de carreras
  //* ------------------------------------------------------------------------------------------------------------------------

  app.post(`${apiRoot}/createCareer`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await CareerService.createCareer(req.body))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.get(`${apiRoot}/findCareer`, loggedInOnly, async (req, res) => { // nestorfigliuolo.com.ar/pss/api/v1/findCareer?Careername=pepito
    try {
      res.send(await CareerService.findCareer(req.query.name))
    } catch (err) {
      console.log(err)
      res.send(err)
    }
  })

  app.get(`${apiRoot}/findCareerById`, loggedInOnly, async (req, res) => { // nestorfigliuolo.com.ar/pss/api/v1/findCareer?Careername=pepito
    try {
      res.send(await CareerService.findCareerById(req.query.name))
    } catch (err) {
      console.log(err)
      res.send(err)
    }
  })

  app.get(`${apiRoot}/getAllCareers`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await CareerService.findAllCareers())
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/updateCareer`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await CareerService.updateCareer(req.body))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/removeCareer`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await CareerService.removeCareer(req.body.name))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  //* ------------------------------------------------------------------------------------------------------------------------
  //* Llamadas CRUD de unidades academicas
  //* ------------------------------------------------------------------------------------------------------------------------

  app.post(`${apiRoot}/createAcademicUnit`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await AcademicUnitService.createAcademicUnit(req.body))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.get(`${apiRoot}/getAllAcademicUnits`, loggedInOnly, async (req, res) => {
    try {
      res.send(await AcademicUnitService.findAllAcademicUnits())
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.get(`${apiRoot}/getAcademicUnit`, loggedInOnly, async (req, res) => {
    try {
      res.send(await AcademicUnitService.findAcademicUnit(req.query.name))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.get(`${apiRoot}/getAcademicUnitById`, loggedInOnly, async (req, res) => {
    try {
      res.send(await AcademicUnitService.findAcademicUnitById(req.query.id))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/updateAcademicUnit`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await AcademicUnitService.updateAcademicUnit(req.body))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  app.post(`${apiRoot}/removeAcademicUnit`, loggedInOnly, adminOnly, async (req, res) => {
    try {
      res.send(await AcademicUnitService.removeAcademicUnit(req.body.name))
    } catch (err) {
      res.status(500).send(err)
    }
  })

  //* ------------------------------------------------------------------------------------------------------------------------
  //* Llamadas de Auth
  //* ------------------------------------------------------------------------------------------------------------------------

  app.post(`${apiRoot}/login`, passport.authenticate('local'), async (req, res) => {
    try {
      const user = await UserService.findUser(req.session.passport.user)
      res.cookie('role', user.role).send(user)
    } catch (err) {
      console.log(err)
    }
  })

  app.post(`${apiRoot}/logout`, (req, res) => {
    req.logout()
    res.status(200).cookie('role', '').send('Logout')
  })

  app.post(`${apiRoot}/register`, loggedInOnly, adminOnly, async (req, res, next) => {
    try {
      const resu = await UserService.createUser(req.body)
      res.send(resu)
    } catch (err) {
      res.send(err)
    }
  })
}
