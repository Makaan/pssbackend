const mongoose = require('mongoose')

module.exports = () => {
  const schema = new mongoose.Schema({
    name: { type: String, required: true },
    academicUnit: { type: mongoose.Schema.Types.ObjectId, ref: 'AcademicUnit', required: true },
    professor: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    assistant: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    careers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Career' }],
    weakCorrelatives: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }],
    strongCorrelatives: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }],
    students: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
  })
  return mongoose.model('Subject', schema)
}
