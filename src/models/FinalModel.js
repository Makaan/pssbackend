const mongoose = require('mongoose')

module.exports = () => {
  const schema = new mongoose.Schema({
    date: { type: Date, required: true },
    students: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    professor: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    course: { type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }
  })
  return mongoose.model('Final', schema)
}
