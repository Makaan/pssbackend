const mongoose = require('mongoose')

module.exports = () => {
  const schema = new mongoose.Schema({
    name: { type: String, required: true },
    courses: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }]
  })
  return mongoose.model('AcademicUnit', schema)
}
