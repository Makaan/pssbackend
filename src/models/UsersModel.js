const mongoose = require('mongoose')

module.exports = (autoIncrement) => {
  const schema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    role: { type: String, required: true }, //! Tipos posibles: student, professor, admin
    name: { type: String, required: true },
    lastName: { type: String, required: true },
    birthDate: { type: Date, required: true },
    birthPlace: { type: String, required: true },
    dni: { type: Number, required: true },
    school: { type: String },
    email: { type: String, required: true },
    address: { type: String, required: true },
    tel: { type: String, required: true },
    lu: { type: Number },
    career: { type: mongoose.Schema.Types.ObjectId, ref: 'Career' },
    // FINALS
    currentSubjects: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }],
    approvedSubjects: [{
      nota: Number,
      materia: { type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }
    }],
    nonApprovedSubjects: [{
      nota: Number,
      materia: { type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }
    }],
    assignedSubjectsAsProfessor: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }],
    assignedSubjectsAsAssistant: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }]
  })
  schema.plugin(autoIncrement.plugin, { model: 'User', field: 'lu', startsAt: 10000 })
  return mongoose.model('User', schema)
}
