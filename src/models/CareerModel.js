const mongoose = require('mongoose')

module.exports = () => {
  const schema = new mongoose.Schema({
    name: { type: String, required: true },
    academicUnit: { type: mongoose.Schema.Types.ObjectId, ref: 'AcademicUnit' },
    since: { type: Date, required: true },
    subjects: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Subject' }]
  })
  return mongoose.model('Career', schema)
}
