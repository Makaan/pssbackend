const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')

module.exports = async () => {
  console.log(process.env.DB_PATH, process.env.DB_USER, process.env.DB_PASS)
  mongoose.connect(process.env.DB_PATH, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    user: process.env.DB_USER,
    pass: process.env.DB_PASS
  })

  mongoose.set('useFindAndModify', false)
  const db = mongoose.connection

  autoIncrement.initialize(db)

  db.on('error', console.error.bind(console, 'connection error:'))

  db.once('open', function () {
    console.log('connected')
  })

  return { db: db, autoIncrement: autoIncrement }
}
