module.exports = () => {
  try {
    const Container = require('typedi').Container
    const PasswordEncryptionService = require('../services/PasswordEncryptionService')
    const UserService = require('../services/UserService')
    const AcademicUnitService = require('../services/AcademicUnitService')
    const CareerService = require('../services/CareerService')
    const SubjectService = require('../services/SubjectService')
    const FinalService = require('../services/FinalService')

    const StudentService = require('../services/StudentService')
    const ProfessorService = require('../services/ProfessorService')

    Container.set('PasswordEncryptionService', new PasswordEncryptionService())
    Container.set('UserService', new UserService())
    Container.set('AcademicUnitService', new AcademicUnitService())
    Container.set('CareerService', new CareerService())
    Container.set('SubjectService', new SubjectService())
    Container.set('FinalService', new FinalService())
    Container.set('StudentService', new StudentService())
    Container.set('ProfessorService', new ProfessorService())
  } catch (err) {
    console.log('Services loader', err)
  }
}
