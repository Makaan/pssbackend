const Container = require('typedi').Container
const userModel = require('../models/UsersModel')
const academicUnitModel = require('../models/AcademicUnitModel')
const careerModel = require('../models/CareerModel')
const subjectModel = require('../models/SubjectModel')
const finalModel = require('../models/FinalModel')

module.exports = (dbProps) => {
  Container.set('UserModel', userModel(dbProps.autoIncrement))
  Container.set('AcademicUnitModel', academicUnitModel())
  Container.set('CareerModel', careerModel())
  Container.set('SubjectModel', subjectModel())
  Container.set('FinalModel', finalModel())
}
