const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const mongoose = require('mongoose')

module.exports = () => {
  app.use(express.json({
    type: ['application/json', 'text/plain']
  }))
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
  app.use(cookieParser('pss'))
  app.use(session({
    cookie: { maxAge: 1000 * 60 * 60 * 24 * 365 },
    store: new MongoStore({
      mongooseConnection: mongoose.connection
    }),
    resave: true,
    saveUninitialized: true,
    secret: 'pss'
  }))

  // Passport login
  const passport = require('./passportLoader')()
  app.use(passport.initialize())
  app.use(passport.session())

  // archivos estaticos
  app.use(express.static('build'))

  // inicializo las rutas de la api
  require('../api/routes')(app)

  app.listen(8081, function () {
    console.log('Example app listening on port 8081!')
  })
}
