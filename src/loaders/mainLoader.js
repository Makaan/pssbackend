module.exports = async () => {
  const mongooseLoader = require('./mongooseLoader')
  const expressLoader = require('./expressLoader')
  const servicesLoader = require('./servicesLoader')
  const modelsLoader = require('./modelsLoader')

  const dbProps = await mongooseLoader()
  console.log('mongodb initialized')

  modelsLoader(dbProps)
  console.log('models loaded')
  servicesLoader()
  console.log('services loaded')
  expressLoader()
  console.log('express initialized')
}
