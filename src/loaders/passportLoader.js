const passport = require('passport')
const passportLocal = require('passport-local')
const Container = require('typedi').Container

module.exports = (app) => {
  const UserService = Container.get('UserService')
  const PasswordEncryptionService = Container.get('PasswordEncryptionService')

  passport.serializeUser((user, done) => {
    done(null, user.username)
  })

  passport.deserializeUser(async (username, done) => {
    const user = await UserService.findUser(username)
    try {
      done(null, user)
    } catch (err) {
      done(err, null)
    }
  })

  const LocalStrategy = passportLocal.Strategy

  const local = new LocalStrategy(async (username, password, done) => {
    try {
      console.log(username, password)
      const user = await UserService.findUser(username)
      console.log(user)
      const isSamePassword = await PasswordEncryptionService.checkPassword(password, user.password)
      if (!user || !isSamePassword) {
        done(null, false, { message: 'Invalid username/password' })
      } else {
        done(null, user)
      }
    } catch (err) {
      done(null, false, { message: 'Invalid username/password' })
      console.log(err)
    }
  })
  passport.use('local', local)

  return passport
}
